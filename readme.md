Logo Web Component with animated (Atari like) gradient rainbow background.  
Uses ES6 modules, compatible with browsers supporting `mask` CSS property.  
## Demo
run `index.html` from `./dist` folder
## How to use:  
Import to your project
```javascript
import { AnimatedLogo } from '@tsabak/animated-logo';
```
or add directly into HTML
```html
<script src="./animated-logo.js" type="module"></script>
```
and add custom element to your html
```html
<animated-logo src-image="./my-logo.svg"></animated-logo>
```
