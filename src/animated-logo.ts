function getWrapperTemplate() {
  const wrapper = document.createElement('div');
  wrapper.style.display = 'flex';
  wrapper.style.width = '100%';
  wrapper.style.height = '100%';
  wrapper.style.alignItems = 'center';
  return wrapper;
}

function getStyles(url: string): string {
  return `
    :host {
      display: flex;
      align-items: center;
      justify-content: center;
      width: 100%;
      height: 100%;
      mask: url(${url}) no-repeat;
      -webkit-mask: url(${url}) no-repeat;
      mask-position: center;
      -webkit-mask-position: center;
      mask-size: contain;
      -webkit-mask-size: contain;
    }
    `;
}
const SEGMENTS_NO = 13;

export class AnimatedLogo extends HTMLElement {
  private colors = [
    // tslint:disable:max-line-length
    ['#4c8fa2', '#84c5d7', '#a5e3fa', '#ace1ff', '#a5dafa', '#96d4f9', '#86d3ff', '#7dcfff', '#79cbfd', '#6bc6ff', '#5fbcf7', '#52aeed', '#469ee5', '#3b93dd', '#3083cf', '#2c71be', '#265dad', '#18519e', '#1e4389', '#3f528a'],
    ['#7888bc', '#abb6ee', '#c6c9ff', '#c7c3fe', '#bab6ff', '#b0afff', '#a9a8f8', '#a19ef7', '#9592f9', '#8e8afb', '#8580f4', '#7b76ea', '#716ce0', '#6762d6', '#6059cd', '#554ec2', '#443cb5', '#322c98', '#302f8d', '#5353b3'],
    ['#8a8cdf', '#b6c0fb', '#c3d1ff', '#b7c7fb', '#a8b6fd', '#9eaefc', '#95a7fb', '#8c9ffe', '#8492fb', '#7989fa', '#6e7dfa', '#6172fe', '#5668f8', '#4d5eea', '#4050e4', '#3443da', '#2536c2', '#202dad', '#283ab6', '#5b62d7'],
    ['#b290ef', '#e3aaf9', '#e2abfe', '#df9efc', '#db91ff', '#d388fe', '#ca74f9', '#c36bf3', '#bc65e8', '#b55fde', '#af56d6', '#a54ccc', '#9540c1', '#8934b6', '#7a25a6', '#6b1697', '#6b1697', '#691398', '#822dac', '#b864c2'],
    ['#ea96e3', '#fbafef', '#ffaaee', '#fca3e7', '#f89cdd', '#f991de', '#f684d9', '#f078d1', '#e76fc6', '#e262c3', '#d858b9', '#cc4cab', '#be419c', '#ae348b', '#9a2377', '#871167', '#7a055d', '#810c64', '#9f4582'],
    ['#d091ae', '#f3bcd3', '#ffc1d6', '#feb9be', '#feafb2', '#fba7a7', '#fe9792', '#ff8a88', '#fe7f78', '#ff7268', '#fb675b', '#f5594c', '#f14a38', '#ed3a23', '#e22e15', '#c22810', '#a22006', '#9a1a01', '#9b371d'],
    ['#b2765e', '#dcb69f', '#ffcdb8', '#ffcbbd', '#fbbfb4', '#fcb4a5', '#fca68f', '#fe9b7e', '#fe8f73', '#fb8665', '#f57d58', '#ee744f', '#ed6146', '#df5236', '#d44326', '#c03a1f', '#ab3119', '#a0230f', '#952c0e', '#945c2d'],
    ['#ce926d', '#f7cc97', '#ffdf96', '#fdda88', '#fbd17d', '#fdc669', '#fdbe55', '#fab74e', '#fbad41', '#fda331', '#f89726', '#f58b27', '#ee7a2d', '#e06b1c', '#d05a1a', '#ba4824', '#a6341a', '#942c0f', '#824117'],
    ['#a17d4b', '#dcbd8e', '#ffed91', '#fffe7f', '#fbfd76', '#fdf169', '#fcec5b', '#f9e552', '#fcd748', '#fcc93c', '#fcbd32', '#fab126', '#f69e19', '#f08b07', '#da7c02', '#b5680e', '#9b520d', '#863f01', '#6c3200', '#784911'],
    ['#b18348', '#e5b468', '#fcc96e', '#fcca6b', '#fabf65', '#feb45f', '#fbad5b', '#eea654', '#e29a4e', '#d78f43', '#cd833c', '#c57530', '#bb6623', '#ad5818', '#9b491a', '#853417', '#712106', '#541b00', '#4e300c', '#755d39'],
    ['#bca16c', '#f2e596', '#f4e28e', '#fbe484', '#f9db69', '#f1d45c', '#ebcd51', '#e3c644', '#ddc034', '#d2b62e', '#c6a92b', '#b89a20', '#a78a12', '#8f760f', '#786016', '#654e08', '#504100', '#494005', '#59561f', '#8d8d43'],
    ['#cbdb57', '#e0e972', '#d6ea65', '#cde35d', '#c3d953', '#b9cc4e', '#b1c542', '#aabb39', '#9eaf2f', '#8e9f1d', '#7d8f15', '#718309', '#667905', '#5a6c06', '#506101', '#425400', '#354600', '#384404', '#50681e'],
    ['#78ac48', '#9de371', '#b6f08a', '#a1f778', '#8ff563', '#88ee5c', '#83df56', '#76cb49', '#6abd3b', '#5ab52c', '#4dac1e', '#41a215', '#3b9a0c', '#348d0b', '#2a8305', '#237300', '#205f06', '#194d02', '#25550b' ],
    ['#56ad24', '#71f7a4', '#7bf4ce', '#5feab5', '#44e099', '#3add93', '#3adc92', '#38d88f', '#34d088', '#30c781', '#2cbf77', '#27b36c', '#22a760', '#1d9b53', '#188f47', '#13833b', '#0f7831', '#0b6f28', '#0a6123', '#0a6123'],
    ['#2b7c60', '#63ecfa', '#76f7fe', '#85fefd', '#83fefd', '#77f8fd', '#67f0fa', '#5be8f5', '#54e2f1', '#4ddaeb', '#46d2e6', '#3fc9de', '#38bed5', '#31b4cc', '#2ca8c2', '#269ab6', '#1a7d9b', '#157190', '#116586'],
  ];
  private canvas: HTMLCanvasElement = document.createElement('canvas');
  private ctx = this.canvas.getContext('2d')!;
  private currentPosition: number = 0;
  private step: number = 0;
  private now: number = 0;
  private running: boolean = true;
  private lastAnimationId: number = 0;

  static get observedAttributes() {
    return ['src-image'];
  }

  constructor() {
    super();
    const shadow = this.attachShadow({ mode: 'open' });

    setTimeout(() => {
      const imageUrl = this.hasAttribute('src-image')
        ? this.getAttribute('src-image')!
        : null;

      const wrapper = getWrapperTemplate();

      wrapper.appendChild(this.canvas);
      const styleEl = document.createElement('style');
      styleEl.textContent = getStyles(imageUrl!);

      shadow.appendChild(styleEl);
      shadow.appendChild(wrapper);
      this.canvas.width = wrapper.clientWidth;
      this.canvas.height = wrapper.clientHeight;
      this.step = Math.floor(this.canvas.height / SEGMENTS_NO);

      wrapper.addEventListener('click', (e) => {
        e.stopPropagation();
        this.running = !this.running;
        if (this.running) {
          this.runAnimation();
        }
      });
      this.runAnimation();
    }, 50);
  }

  public disconnectedCallback() {
    this.running = false;
    cancelAnimationFrame(this.lastAnimationId);
  }

  private displayGradient(currentPos: number) {
    let idx = 0;
    for (let i = 0 + currentPos - this.step; i < this.canvas.height; i += this.step) {
      const gradient = this.createGradient(i, idx, this.step);
      this.ctx.fillStyle = gradient;
      this.ctx.fillRect(0, i < 0 ? 0 : i, this.canvas.width, i + this.step);
      if (idx >= this.colors.length) {
        idx = 0;
      } else {
        idx += 1;
      }
    }
  }

  private createGradient(posY: number, idx: number, step: number): CanvasGradient {
    const gradient = this.ctx.createLinearGradient(0, posY, 0, posY + step);
    for (let i = 0; i < this.colors[idx].length; i++) {
      gradient.addColorStop(i / this.colors[idx].length, this.colors[idx][i]);
    }
    return gradient;
  }

  private runAnimation(timestamp?: number) {
    this.now = timestamp || 0;
    if (this.running) {
      this.lastAnimationId = requestAnimationFrame(this.runAnimation.bind(this));
    }

    this.displayGradient(this.currentPosition);

    if (this.currentPosition >= this.step) {
      this.currentPosition = 0;
      this.colors.unshift(this.colors.pop()!);
    } else {
      this.currentPosition++;
    }
  }

}

customElements.define('animated-logo', AnimatedLogo);
